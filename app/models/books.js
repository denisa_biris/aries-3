'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

const bookSchema = new Schema({
    createdAt: Number,
    updatedAt: Number,
    name: {
        type: String,
        required: true,
        unique: false
    },
    author: {
        type: String,
        required: true,
        unique: false
    },
    user: {
        type: ObjectId,
        ref: 'user',
        required: true
    }
}, {
    timestamps: { currentTime: () => new Date().getTime()}
});

module.exports = mongoose.model("book", bookSchema, "books");
