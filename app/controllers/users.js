'use strict'

const User = require("../models/users");

module.exports = {
    isAdmin: isAdmin,
    getUsers: getUsers,
    getUsersById: getUsersById,
    createUser: createUser,
    putUser: putUser,
    deleteUser: deleteUser,
    responseToJSON: responseToJSON
};

function isAdmin(req, res, next) {
    const isAdminVal = true;

    if(isAdminVal){
        return next();
    }
    return next({statusCode: 401, message: "Unauthorized!"});
}

function getUsers(req, res, next){
    const filter = {};
    if(req.query.isActive){
        filter.isActive = req.query.isActive;
    }
    if(req.query.role){
        filter["details.role"] = req.query.role;
    }
   User.find(filter, function(err, result){
       if(err){
           // return res.json(err);
           return next(err);
       }
       req.resources.users = result;
       return next();
   });
}

function getUsersById(req, res, next){
    User.find({_id: req.params.id}, function(err, result){
        if(err){
            // return res.json(err);
            return next(err);
        }
        req.resources.users = result;
        return next();
    });
}

function createUser(req, res, next){
    console.log("all users before create" , req.resources.users);
    ////////////////////////////////
    const addUser = req.body;
    addUser.details = JSON.parse(req.body.details);
    addUser.documents = JSON.parse(req.body.documents);
    const user = new User(req.body);

    user.save(function(err, result){
        if(err){
            // return res.json(err);
            err.statusCode = 409;
            return next(err);
        }
        req.resources.addUsers = result;
        return next();
    });
}

function putUser(req, res, next){
    User.findByIdAndUpdate(req.params.id, req.body, function(err, result){
        if(err){
            // return res.json(err);
            return next(err);
        }
        req.resources.users = result;
        return next();
    });
}

function deleteUser(req, res, next){
    User.deleteOne({_id: req.params.id}, function(err, result){
        if(err){
            // return res.json(err);
            return next(err);
        }
        req.resources.users = result;
        return next();
    });
}

function responseToJSON(prop) {
    return function(req, res, next){
        return res.json(req.resources[prop]);
    }
}