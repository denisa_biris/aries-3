'use strict'

const multer = require('multer');
const path = require('path');

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        let pathPrefix = path.resolve(__dirname, '../files');
        cb(null, pathPrefix);
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname );
    }
});

module.exports = {
    storage: storage,
    uploadFile: uploadFile,
    responseToJSON: responseToJSON
}

function uploadFile(req, res, next){
    console.log("the file to upload", req.file);
    req.resources.upload = {upload: true, fileName: req.file.filename};
    return next();
}

function responseToJSON(prop) {
    return function(req, res, next){
        return res.json(req.resources[prop]);
    }
}