'use strict'

const Book = require("../models/books");

module.exports = {
    getBooks: getBooks,
    getBooksById: getBooksById,
    createBook: createBook,
    putBook: putBook,
    deleteBook: deleteBook,
    responseToJSON: responseToJSON
};

function getBooks(req, res, next){
    Book
        .find()
        .sort({name: 1})
        .populate('user', 'email name details.role')
        .exec(function(err, result){
            if(err){
                // return res.json(err);
                return next(err);
            }
            req.resources.books = result;
            return next();
        });
}

function getBooksById(req, res, next){
    Book.find({_id: req.params.id}, function(err, result){
        if(err){
            // return res.json(err);
            return next(err);
        }
        req.resources.books = result;
        return next();
    });
}

function createBook(req, res, next){
    const book = new Book(req.body);

    book.save(function(err, result){
        if(err){
            // return res.json(err);
            return next(err);
        }
        req.resources.books = result;
        return next();
    });
}

function putBook(req, res, next){
    Book.findByIdAndUpdate(req.params.id, req.body, function(err, result){
        if(err){
            // return res.json(err);
            return next(err);
        }
        req.resources.books = result;
        return next();
    });
}

function deleteBook(req, res, next){
    Book.deleteOne({_id: req.params.id}, function(err, result){
        if(err){
            // return res.json(err);
            return next(err);
        }
        req.resources.books = result;
        return next();
    });
}

function responseToJSON(prop) {
    return function(req, res, next){
        return res.json(req.resources[prop]);
    }
}