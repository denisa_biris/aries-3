'use strict'

const express = require('express');
const router = express.Router();
const usersCtrl = require('../controllers/users')

router.get('/users',
    usersCtrl.isAdmin,
    usersCtrl.getUsers,
    usersCtrl.responseToJSON("users")
);

router.get('/users/:id',
    usersCtrl.getUsersById,
    usersCtrl.responseToJSON("users")
);

router.post('/users',
    usersCtrl.getUsers,
    usersCtrl.createUser,
    usersCtrl.responseToJSON("addUsers")
);

router.put('/users/:id',
    usersCtrl.putUser,
    usersCtrl.responseToJSON("users")
);

router.delete('/users/:id',
    usersCtrl.deleteUser,
    usersCtrl.responseToJSON("users")
);

module.exports = router;