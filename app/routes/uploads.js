'use strict'

const express = require('express');
const multer = require('multer');
const path = require('path');
const router = express.Router();
const uploadCtrl = require('../controllers/uploads');

var upload = multer({ storage: uploadCtrl.storage });

router.post('/upload',
    upload.single('avatar'),
    uploadCtrl.uploadFile,
    uploadCtrl.responseToJSON("upload")
);

router.get('/download',
    function(req, res, next) {
        let filename = req.query.filename;
        let pathPrefix = path.resolve(__dirname, `../files/${filename}`);
        res.download(pathPrefix);
    }
);

module.exports = router;