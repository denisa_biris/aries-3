'use strict'

const express = require('express');
const router = express.Router();
const booksCtrl = require('../controllers/books')

router.get('/books',
    booksCtrl.getBooks,
    booksCtrl.responseToJSON("books")
);

router.get('/books/:id',
    booksCtrl.getBooksById,
    booksCtrl.responseToJSON("books")
);

router.post('/books',
    booksCtrl.createBook,
    booksCtrl.responseToJSON("books")
);

router.put('/books/:id',
    booksCtrl.putBook,
    booksCtrl.responseToJSON("books")
);

router.delete('/books/:id',
    booksCtrl.deleteBook,
    booksCtrl.responseToJSON("books")
);

module.exports = router;