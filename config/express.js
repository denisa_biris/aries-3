'use strict'

module.exports = {
    initExpress: initExpress
}

const bodyParser = require("body-parser");

function initExpress(app){
    app.use(bodyParser.urlencoded({extended: false}));

    //parse application/json
    app.use(bodyParser.json());

    app.use(function(req, res, next){
        console.log("generic midd", req.method, req.url);
        req.resources = req.resources || {};
        next();
    });
}